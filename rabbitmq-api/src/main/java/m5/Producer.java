package m5;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");  // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();
        // 创建 direct 类型交换机： direct_logs
        c.exchangeDeclare("topic_logs", BuiltinExchangeType.TOPIC);
        // 发送消息，消息上需要携带路由键关键词
        while (true) {
            System.out.print("输入消息： ");
            String s = new Scanner(System.in).nextLine();
            System.out.print("输入路由键： ");
            String k = new Scanner(System.in).nextLine();
            // 对于默认交换机，使用队列名作为路由键
            c.basicPublish("topic_logs", k, null, s.getBytes());
        }

    }
}
