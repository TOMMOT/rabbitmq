package m2;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection();
        Channel c = con.createChannel();

        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String s, Delivery message) throws IOException {
                byte[] a = message.getBody();
                String ss = new String(a);
                System.out.println("收到: "+ss);
                for (int i = 0; i < ss.length(); i++) {
                    if ('.' == ss.charAt(i)) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                    }
                }
                //发送回执
                // c.basicAck(回执, 是否对之前收到的所有消息一起进行确认);
                c.basicAck(message.getEnvelope().getDeliveryTag(),false);
                System.out.println("---------------------------消息处理结束");
            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String s) throws IOException {

            }
        };
        c.basicConsume("hello-world", false,deliverCallback,cancelCallback);
    }
}
