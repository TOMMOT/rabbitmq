package cn.tedu.rabbitmqspring.m4;


import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Producer {
    @Autowired
    private AmqpTemplate t;

    public void send(){
        while(true){
            System.out.println("input msg : ");
            String s = new Scanner(System.in).nextLine();
            System.out.println("input key : ");
            String k = new Scanner(System.in).nextLine();
            t.convertAndSend("direct_logs",k,s);
        }
    }

}
