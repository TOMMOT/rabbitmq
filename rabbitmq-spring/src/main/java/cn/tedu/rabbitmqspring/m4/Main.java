package cn.tedu.rabbitmqspring.m4;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;


@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public DirectExchange logsExchange() {
        return new DirectExchange("direct_logs", false, false);
    }

    // 添加测试代码，调用生产者发送消息
    @Autowired
    private Producer p;
    /*
      spring 的执行流程：
      包扫描创建所有实例 --> 完成所有的依赖注入 --> @PostConstruct --> 后续流程自动配置类，消费者.....
     */
    @PostConstruct
    public void test() {
        // new Thread(new Runnable() {
        //     @Override
        //     public void run() {
        //         p.send();
        //     }
        // }).start();

        // lambda表达式，是匿名内部类的简化语法
        new Thread(() -> p.send()).start();
    }
}
