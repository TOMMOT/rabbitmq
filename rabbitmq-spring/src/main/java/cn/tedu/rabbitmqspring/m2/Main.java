package cn.tedu.rabbitmqspring.m2;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;


@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public Queue taskQueue(){
        return new Queue("task_queue",false);
    }

    @Autowired
    private Producer p;

    @PostConstruct
    public void test(){
        new Thread(()->{
            p.send();
        }).start();
    }
}
