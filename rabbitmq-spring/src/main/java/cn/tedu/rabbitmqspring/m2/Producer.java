package cn.tedu.rabbitmqspring.m2;


import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Producer {
    @Autowired
    private AmqpTemplate t;

    public void send(){
        while(true){
            String s = new Scanner(System.in).nextLine();
            t.convertAndSend("task_queue",s.getBytes());
        }
    }

}
