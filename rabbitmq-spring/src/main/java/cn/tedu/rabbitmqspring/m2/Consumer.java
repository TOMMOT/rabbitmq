package cn.tedu.rabbitmqspring.m2;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @RabbitListener(queues = "task_queue")
    public void receive1(String s){
        System.out.println("receive1 收到： "+s);
    }
    @RabbitListener(queues = "task_queue")
    public void receive2(String s){
        System.out.println("receive2 收到： "+s);
    }

}
