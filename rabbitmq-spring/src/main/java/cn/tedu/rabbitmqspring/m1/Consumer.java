package cn.tedu.rabbitmqspring.m1;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @RabbitListener(queues = "helloWorld")
    public void receive(String s){
        System.out.println("收到： "+s);
    }

}
