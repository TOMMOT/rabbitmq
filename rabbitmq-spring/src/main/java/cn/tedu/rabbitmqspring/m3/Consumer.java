package cn.tedu.rabbitmqspring.m3;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @RabbitListener(bindings = @QueueBinding(value = @Queue, exchange =@Exchange(name = "logs",declare = "false")))
    public void receive1(String s){
        System.out.println("receive1 get: "+s);
    }
    @RabbitListener(bindings = @QueueBinding(value = @Queue, exchange =@Exchange(name = "logs",declare = "false")))
    public void receive2(String s){
        System.out.println("receive2 get: "+s);
    }

}
